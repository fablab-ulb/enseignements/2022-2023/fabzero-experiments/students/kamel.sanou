# 4. Outil Fab Lab sélectionné
Cette semaine, j'ai choisi la formation microcontrôleur. Le mi en fait c'est comme 
un petit ordi. C'est un circuit intégré auquel tu peux donner des commandes et il effectue 
des choses pour toi. Du genre tu lui donnes un code et il allume des leds chaque 10 secondes.  
Nous avons travaillé avec un YD-RP2040 qui se rapproche d'un raspberry pico. Ca ressemble à ça  
![](images/mod4/pico.jpg)

## Choix du language/bibliothèque
Le language utilisé pour controller la petite machine est soit :
* Micropython avec [Thonny]()
* Arduino avec [Aeduino]()
* [C]()  

## 1. Micropython

### 1.1 Configuration
J'ai écrit en micropython car j'avais déjà le logiciel **Thonny** et quelques notions en python. Il a fallu faire une miniconfiguration 
d'abord:  
(Toutes les infos complètes peuvent être trouvées
 sur [ce guide](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/))  
 - Brancher le µcontrôleur en maintenat le bouton boot appuyé.... **hehe en maintenant le** 
 **BOOT-ON appuyé**  
 ![boot](images/mod4/boot2.jpg)
 - Sur **Thonny** aller dans Tools->Options->Interpreter et choisir Raspberry pico puis 
installer 
 ![](images/mod4/options.jpg) 
 - Tu peux maintenant écrire ton code et faire marcher ton µcontrôleur.
 
 ### 1.2 Codes
Le µcontrôleur pico sait faire plein de choses quand il est couplé à d'autres mini appareils.
Jusque là, utilisé tout seul, je sais allumer/faire clignoter ses lumières intégrées.  

 | `import machine`| toujours le faire pour que la machine soit ...prise en compte|
 |-----------------|--------------------------------------------------------------|
 |`import time`| tu vas en avoir besoin pour reguler le temps que passent les lumières allumées par ex|
 |`from machine import Pin`|importer le module "Pin"|
 |`led=Pin(25, mode= Pin.OUT)`|associe à la led qui se trouve à la sortie 25 ( à gauche du port usb) , la variable nommée led|
 |`led.value(True)`ou `led.value(1)`|allume la led|
 |`led.value(False)` ou `led.value(0)`|éteint la led|
  
#### Exemple de code 
Celui là fait clignoter 5 fois le Pin 25 
```
import machine
import time
from machine import Pin
led = Pin(25, mode=Pin.OUT)

counter =0
while (counter<5):
    led.value(True)
    time.sleep(1)
    led.value(False)
    time.sleep(0.5)
    counter+=1 
```
[Sur ce site](https://docs.micropython.org/en/latest/esp8266/tutorial/neopixel.html), y a d'autres codes

#### Un autre exemple
Une led nous était fournie dans le kit. J'ai connecté le court pied de la led à une resistance de 120 ohm je crois.
le long pied à la Pin 19 du µcontrôleur et l'autre pied de la resistanceau ground et je lance le code:
```
import machine
from machine import Pin
led = Pin(19, Pin.OUT)
led.value(True)
```
![](images/mod4/lampmontage.jpg) ![](images/mod4/led.jpg)

## 1.3 Infos utiles
Lorsque tu run ton code sur Thonny, ça te demande de le run soit sur ton pc soit sur le µcontrôleur. 
Le controleur ne peut run que les fichiers qui s'appelent "main". Faudrait donc tout le temps créer 
un autre fichier qui s'appelera "main" (et écraser celui déjà existant du coup...).   
Tu peux néanmoins
 créer, enregistrer et run tes fichiers sous le nom que tu veux sur ton pc et ça marche. 

## 2. Assignment

### 2.1 dht20
C'est un senseur d'hummidité et de température. Pour plus d'infos, c'est par 
[là](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf).  
 Il fallait le connecter au µcontrôleur et écrire quelques codes sur Thonny.
 
#### 2.1-1 Codes 
Toutes les infos se trouvent [ici](https://github.com/flrrth/pico-dht20)  
Faut d'abord dans un fichier à part **nommé dht20** écrire ce code:  
```
# https://github.com/flrrth/pico-dht20

from machine import I2C
from utime import sleep_ms


class DHT20:
    """Class for the DHT20 Temperature and Humidity Sensor.
    The datasheet can be found at http://www.aosong.com/userfiles/files/media/Data%20Sheet%20DHT20%20%20A1.pdf
    """
    
    def __init__(self, address: int, i2c: I2C):
        self._address = address
        self._i2c = i2c
        sleep_ms(100)
        
        if not self.is_ready:
            self._initialize()
            sleep_ms(100)
            
            if not self.is_ready:
                raise RuntimeError("Could not initialize the DHT20.")
        
    @property
    def is_ready(self) -> bool:
        """Check if the DHT20 is ready."""
        self._i2c.writeto(self._address, bytearray(b'\x71'))
        return self._i2c.readfrom(self._address, 1)[0] == 0x18
    
    def _initialize(self):
        buffer = bytearray(b'\x00\x00')
        self._i2c.writeto_mem(self._address, 0x1B, buffer)
        self._i2c.writeto_mem(self._address, 0x1C, buffer)
        self._i2c.writeto_mem(self._address, 0x1E, buffer)
    
    def _trigger_measurements(self):
        self._i2c.writeto_mem(self._address, 0xAC, bytearray(b'\x33\x00'))
        
    def _read_measurements(self):
        buffer = self._i2c.readfrom(self._address, 7)
        return buffer, buffer[0] & 0x80 == 0
    
    def _crc_check(self, input_bitstring: str, check_value: str) -> bool:
        """Calculate the CRC check of a string of bits using a fixed polynomial.
        
        See https://en.wikipedia.org/wiki/Cyclic_redundancy_check
            https://xcore.github.io/doc_tips_and_tricks/crc.html#the-initial-value
        
        Keyword arguments:
        input_bitstring -- the data to verify
        check_value -- the CRC received with the data
        """
        
        polynomial_bitstring = "100110001"
        len_input = len(input_bitstring)
        initial_padding = check_value
        input_padded_array = list(input_bitstring + initial_padding)
        
        while '1' in input_padded_array[:len_input]:
            cur_shift = input_padded_array.index('1')
            
            for i in range(len(polynomial_bitstring)):
                input_padded_array[cur_shift + i] = \
                    str(int(polynomial_bitstring[i] != input_padded_array[cur_shift + i]))
                
        return '1' not in ''.join(input_padded_array)[len_input:]
        
    @property
    def measurements(self) -> dict:
        """Get the temperature (°C) and relative humidity (%RH).
        
        Returns a dictionary with the most recent measurements.
        't': temperature (°C),
        't_adc': the 'raw' temperature as produced by the ADC,
        'rh': relative humidity (%RH),
        'rh_adc': the 'raw' relative humidity as produced by the ADC,
        'crc_ok': indicates if the data was received correctly
        """
        self._trigger_measurements()
        sleep_ms(50)
        
        data = self._read_measurements()
        retry = 3
        
        while not data[1]:
            if not retry:
                raise RuntimeError("Could not read measurements from the DHT20.")
            
            sleep_ms(10)
            data = self._read_measurements()
            retry -= 1
            
        buffer = data[0]
        s_rh = buffer[1] << 12 | buffer[2] << 4 | buffer[3] >> 4
        s_t = (buffer[3] << 16 | buffer[4] << 8 | buffer[5]) & 0xfffff
        rh = (s_rh / 2 ** 20) * 100
        t = ((s_t / 2 ** 20) * 200) - 50
        crc_ok = self._crc_check(
            f"{buffer[0] ^ 0xFF:08b}{buffer[1]:08b}{buffer[2]:08b}{buffer[3]:08b}{buffer[4]:08b}{buffer[5]:08b}",
            f"{buffer[6]:08b}")
        
        return {
            't': t,
            't_adc': s_t,
            'rh': rh,
            'rh_adc': s_rh,
            'crc_ok': crc_ok
        }
```
**JE PRECISE QUE J'EN SUIS PAS L'AUTEUR!!!!**  
et dans un autre écrire celui là:
```
from machine import Pin, I2C
from utime import sleep

from dht20 import DHT20


i2c0_sda = Pin(8)
i2c0_scl = Pin(9)
i2c0 = I2C(0, sda=i2c0_sda, scl=i2c0_scl)

dht20 = DHT20(0x38, i2c0)

while True:
    measurements = dht20.measurements
    print(f"Temperature: {measurements['t']} °C, humidity: {measurements['rh']} %RH")
    sleep(1)
```
**JE SUIS TOUJOURS PAS L'AUTEUR!!!**

#### 2.1-2 Branchement
Après cette partie, faut connecter le senseur et le µcontrôleur par l'intermédiaire de la breadbox.
![branchement](images/mod4/dht2.jpg)


#### 2.1-3 Infos random
Quelques infos random que j'ai notées sur le fonctionnement des µcontrôleur et dht20:  
3,3V comme limite=>si on met plus ça va péter.
Donc faut mettre des résistances comme ça on est bon. ou bien des transistors? y a des exemples de montage sur le site  
Diode evite des problèmes avec l'USB...séparer les deux alimentations, bloque les retours de courants/courant de ...flyback?   
Chaque sortie tire 12mA mais par défaut 2mA et au total 50mA sur toutes les sorties (ce qui est pas bcp...)  
Après des calculs on trouve une résistance de 150 ohm.
Si on a des composants passifs à mettre en entrée, faut des ponts diviseurs.  
Resolution 0,01 avec une accuracy de 0,5 =>pas très efficace... sauf si on veut observer de très petites variations de (T°/humidité?) 
là on peut voir une tendance

### 2.2 VMA309 module de détection de son avec microphone
 Le but est de pouvoir configurer un senseur de son choix. Moi j'ai choisi du coup le VMA309 et il
 ressemble à ça:   
 ![vma](images/mod4/vma309.jpg)  
 Le site sur lequel il est possible d'[acheter](https://www.velleman.eu/products/view/?id=435532) le VMA309 présente un [guide et quelques
 instructions](https://www.velleman.eu/downloads/29/vma309_a4v02.pdf) sur
 comment configurer et brancher le capteur. Toutefois il est en **arduino**.  
 Autant que je ne veux pas l'admettre, j'ai utilisé chatGPT pour convertir d'Arduino en µpython. 
 Voici le code de départ:
 ```
 int sensorPin =A5;
void setup ()
{
    serial.begin (9600);
}

void loop ()
{
    sensorValue = analogRead (sensorPin);
    
    delay (500);
    Serial.println (sensorValue, DEC);
}
 ```
 et Chatgpt m'a donné
 ```
 import machine

from machine import Pin, ADC
import time

sensorPin = ADC(Pin(5))
sensorPin.atten(ADC.ATTN_11DB) # sélectionne la plage de tension d'entrée

while True:
    sensorValue = sensorPin.read()
    print(sensorValue)
    time.sleep_ms(500)
 ```
 **C'est pas un bon code.**
 Y avait quand même quelques erreurs, j'ai dû préciser que je travaille avec un YD-RP2040 et un VMA309 car 
 le PIN 5 ne fonctionne pas en ADC. Plus d'[infos sur l'ADC](https://docs.micropython.org/en/latest/library/machine.ADC.html#machine-adc)
 ...bref j'ai utilisé celui là:
 ```
 from machine import Pin, ADC
import time

sensorPin = ADC(Pin(26))

while True:
    sensorValue = sensorPin.read_u16()
    print(sensorValue)
    time.sleep(0.5)
 ```
 Mais je sais pas trop quelles mesures il me donnait. J'ai donc demandendé de me le modifier en code 
 qui donne la **fréquence**.  
 J'ai eu celui là :
 ```
 import machine
import time

sensor_pin = machine.Pin(26, machine.Pin.IN)
last_tick = time.ticks_us()

while True:
    current_tick = time.ticks_us()
    duration = time.ticks_diff(current_tick, last_tick)
    if duration > 0:
        frequency = 1000000 / duration
        print("Frequency: %.2f Hz" % frequency)
    last_tick = current_tick

 ```
 
#### Faut noter que
``frequency= 1000000/duration`` car ``time.ticks`` est en µ-secondes du coup faut reconvertir.  
le ``if`` sert à fixer une fréquence seuil.  
LE CODE SEMBLLE NE PAS MARCHER....**Que j'enlève ou pas le capteur, j'obitens les même valeurs**!!!  
![task failed](images/mod4/task.jpg)

#### Le montage ressemble à ça (#mêmesiçamarchepas)
![cont](images/mod4/vmaplaque.jpg)
![capt](images/mod4/vmamontage.jpg)  
avec: 
 
 | µcontrôleur| VMA309|  
 |------------|-------|  
 |GND         |   G   |   
 |3,3V        |    +   |  
 |GP26 (qui s'appelle aussi A0 j'aurais pu m'en rendre compte)|A0|



##  3. Checklist
- documented what you have learned from interfacing input devices to a 
 micro controller and how the physical property relates to the measured results  
- (optional) documented what you have learned from interfacing an output 
 device to a micro controller and controlling the device  
- ☑ programmed your board  
- ☑ described your programming process  
- ☑ outlined problems and how you fixed them  
- ☑ included original design files and source code  
- ☐ included a "hero shot/video" of your board in operation