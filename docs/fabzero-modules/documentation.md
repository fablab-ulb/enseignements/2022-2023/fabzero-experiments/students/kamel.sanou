# Semaine 1: Gestion de projet et documentation

# 1. Documentation 
 Cette semaine , j'ai appris à écrire un blog et à faire une documentation.  
 La documentationest grave importante pour te permettre de facilement te retrouver quand tu sais plus quelle commande 
 effectuer. Elle aide aussi les autres qui sont bloqués.  
 Cette image devrait vous convaincre à faire une documentation  
 
 ![](images/sexref.jpg)  
 
 Y a un [bouton "magique"](https://imgur.com/t/rick_roll/ijTYytO) qui te permet de faire ta documentation en un clic, ça m'a 
 beaucoup aidé.

 
# 2. Installations et Configurations

On écrit notre blog en markdown car le language est plus pratique/facile qu'en html. 

Il faut un terminal, j'ai donc installé [git...comprenant gitBash du coup](https://git-scm.com/download/win).    
 
Il faut aussi installer un éditeur de texte pour y écrire en markdown du coup. Moi j'ai installé [Notepad++](https://notepad-plus-plus.org/downloads/) car
le nom me sembalit plus cool que visual studio code.  

## 2.1 Notepad++

Après son installation, faire les [tutos](https://www.markdowntutorial.com/lesson/1/) pour apprendre à écrire en markdown
Il y en a aussi sur le [site du cours](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/documentation.md).  
Je conseillerais de commencer à écrire sa documentation dans un fichier sur notepad++ et pas attendre la dernière
minute pour le faire. Sinon ça bouffe trp de temps   
Moi j'étais largué au départ et du coup j'ai tout noté dans un carnet. Au bout d'un certain temps on commence 
à adapter un rythme. Moi je reportait 4-5 fois par semaine de mon carnet sur notepad++.
 
### 2.1.1 Configurer notepad++
 J'aimerais juste pouvoir voir si ce que j'écris dans notepad++ s'affiche de la bonne manière/ genre pouvoir le visualiser.
 Du coup j'ai installé un plugin qui te laisse utiliser notepad++ en **Markdown** et voir le preview en même temps.  
 Au moment où j'écris ça le plus à jour s'appelle **Markdown Panel**.  
 Pour ça , faut:   
 
 
 **Notepad++ -> Plugins -> Plugin Admin ->** rechercher **Markdown panel -> Install**  
 Normalement faut cliquer sur l'icoône violette tout à droite qui s'ajoute et ça passe...
![](images/mod1/echec.jpg)   
  

 Ca n'a pas marché => en fait il faut que ton fichier sur lequel tu travailles soit enregistré en Markdown
(md)  
Notepadd++ n'enregistre pas directement en md => faut faire **save as -> tous les fichiers -> ajouter l'extension 
 ".md" toi même**  
 Puis réouvrir ton fichier et cliq sur l'icône violette. ![](images/mod1/reussite.jpg).

## 2.2 Configuration de gitlab
Il faut **configurer son espace de travail**  
Pour ça, ouvre ton terminal gitBash et suis [ce tuto](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#convert-a-local-directory-into-a-repository)
![](images/mod1/gitconfig.jpg)
 

## 2.3 Fork
En suivant le [tuto](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#convert-a-local-directory-into-a-repository)
, tu comprends qu'il faut **fork** le repository. Dans notre cas, tu veux fork le 
repository qui commence par fablab ULB et qui se termine par ton prénom.nom.  
![](images/fork.jpg)  
là, c'est bien le 3e que tu veux fork, pas le site de la classe comme je l'ai fait par erreur. C'est pas une fatalité...n'empêche.  

## 2.4 Clé SSH
La clé ssh contient une clé publique et une privée.... **NE PARTAGE PAS LA CLE PRIVEE LOL**  
Tu dois configurer une clé ssh en suivant ce [tutoriel](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account)
.  
Je recommande la clé ED25519 comme le premier exemple qui est fait sur le [tutoriel](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account).  
 A un moment , tu devras entrer une **Passphrase**, c'est comme un mot de passe mais avec une phrase du coup et elle encrypte
 tes keys. Ca réduit les risques de fuites from tes backups.  
 
 
 Tu dois avoir un truc comme ça après avoir généré le ssh  
 
 ![](images/hn1.png) 
 
 ![](images/hn2.png)  
 
### 2.4.1 Ajouter la clé à ton gitlab
 Pour ça tu dois run la commande suivante en fonction de ton système d'exploitation  
![](images/mod1/addkey.jpg)  

## 2.5 Clone
 En gros tu dois cloner ton espace de travail. Tu pourras faire des modifications sur ton clone (qui se trouvera sur ton pc/mac). Du coup, quand tu seras
sûr de tes modifications, tu les uploaderas de ton clone vers ton "original".  
 Retrouve le chemin d'accès de ta clé ssh et ouvre là dans ton éditeur de texte.  
 Le mien par exemple c'etait
_**OC-> Utilisateur -> Kamel -> .ssh**_
  ![](images/mod1/pub.jpg)  
 **SURTOUT N'UTILISE PAS TA CLE PRIVEE**.  
 la clé publique se termine par **pub**.  
 Copie cette clé.  
Puis vas sur **gitlab** -> En haut à droite clique sur ton **avatar** -> **Preferences** -> **SSH keys** et dans le cadre
_**key**_ tu **colles la clé** copiée plus haut -> dans _**description**_ metrre un truc du genre site fablab(
**tu écris juste ce que tu veux**) -> **add key**.
 
 Ensuite tu peux vérifier que tu sais te connecter avec la commande:  
 `ssh -T git@gitlab.com`. ![](images/mod1/verify.jpg) 
 
 Pour faire le clone, sur gitlab, cliquer sur **clone** -> **copie le lien ssh**  
 Dans le terminal,:  
 -> Ouvrir un directory où tu veux que ton clone se retrouve (moi par exemple j'ai créé
 un dossier "Gitlab" dans mon dossier Documents au préalable). Il faut effectuer la commande
 `cd le directory`   
 -> Ecrire `git clone (copie du lien)` ![](images/mod1/ssh.jpg)  
 Git va d'office ouvrir un dossier et mettre le repository dans ton directory.
 

 
# 3. Editer ton site  

## 3.1 Projet
 Sur ce que t'as cloné, se trouvent ces fichiers:  
 
* README.md
* docs
     * images 
     * index
     * fabzero-modules
* .gitignore
* .gitlab-ci.yml
* mkdcs.yml
* requirements  
(Les fichiers qui commencent par un point ne s'afficheront pas dans ton terminal)
 
 

## 3.2 MkDocs
 Faut installer [mkdocs](https://www.mkdocs.org/getting-started/) car ça nous permettra d'avoir un aperçu du site. j'ai essayé la commande `pip install mkdocs`
 mais ça n'a pas marché car j'ai pas **pip**.  
 ->[Telecharger pip](https://www.mkdocs.org/user-guide/installation/) avec la commande `python get-pip.py` mais sans
 python ça marche toujours pas.  
 ->Du coup j'ai installé python et faut faire attention à l'ouvrir en administrateur et cocher le PATH.  
 ->Ofc ça ne marche toujours pas => Windows reconnait pas tout le temps l'argument **python** donc faut écrire **py** plutôt.  
 -> J'arrive tjrs pas à installer pip => pourrai pas installer mkdocs => pas d'aperçu.  
 Finalement j'ai fait un truc mais je sais pas du tout si c'était la bonne chose à faire n'empêche que ça a marché
 pour moi.  
 
##### Ce que j'ai fait 
Sur gitlab, je suis allé sur la page "class-website" et dans le volet _**gitlab-ci.yml**_, j'ai recopié
les 2 commmandes écrites au niveau de _**before_script**_ .  
 Je précise que c'est bien au niveau de class-website et pas au niveau de ta page prénom.nom .
 ![](images/mod1/pip1.jpg)  
 
 Ca m'a aidé à installer pip et [mkdocs](https://www.mkdocs.org/getting-started/) par la même occasion.
 
## 3.3 Modifications à faire
 A partir d'ici, faut ouvrir ton explorateur de fichier, puis ton directory et effectuer tes modifications direct dans ton éditeur de texte.
 A chaque fois que t'as fini un ..bloc... de modifications,dans ton terminal,tu retournes sur **prénom.nom** et tu executes les commandes suivantes:  
 1. `git pull`
 2. `git add *` _(l'etoile sert à tout enregistrer, sinon tu mets juste le nom du fichier. y a un espace entre le add et l'etoile)_
 3. `git commit -m "tu mets ton commentaire ici bg"`
 4. `git push`
 
### 3.3.1 mkdocs.yml
 Tu peux modifier les names, url et le thème.  
 -Le **site url** c'est celui oèu tu veux voir ton blog  
 -Le **repo_url**  c'est celui de ta page sur fablab  
 -Le **theme** t'aide à personnaliser ta page.Moi j'ai installé [windmill-dark](https://github.com/noraj/mkdocs-windmill-dark)
en suivant les instructions. Après l'avoir mis comme thème, il faut te rendre dans ton
dossier **requirements** et écrire sur une autre ligne `mkdocs-windmill-dark`.   
J'imagine que de façon génerale faudra écrire: `mkdocs-"nom du theme"`
![](images/mod1/hmmmm.jpg)  
Toutefois après avoir modifié ce document et fait mon git add j'ai eu un **HEAD DETACHED**... de ce que j'ai
 compris, c'est que ....le fil a été perdu...la tête est détachée.
 ![](images/mod1/tete.jpg)
 
#### Resoudre avec checkout master
 La commande checkout master est outdated => au lieu de `master` écrire `main`  
 Tu dois te mettre dans le dossier où se trouve le fichier qui présente un head detached et effectuer 
 ces commandes:  
 ```
 git branch tmp  
 git checkout main  
 git merge tmp  
 git status  
 git push  
```
...Enter passphrase puis 
  
```git status   
 git branch  
 git branch -D tmp
 ``` 
 J'ai pas su retrouver le site qui m'avait permis de faire ça mais y a [ce site là](https://stackoverflow.com/questions/10228760/how-do-i-fix-a-git-detached-head)
 
### 3.3.2 Index
 C'est ton site, t'écris ce que tu veux.  
 Pour voir provisoirement comment ton blog s'affiche en temps réel, tu vas sur ton terminal -> dans le directory prenom.nom
 -> et tu écris `mkdocs serve`.
 Tu copies le lien qui s'affiche tout en bas et tu le colles dans ton navigateur préféré.    
 
 Beware!!! faire un "ctrl C" va terminer la commande et ton site ne pourras plus s'afficher. Tu dois juste 
 laisser la commande "ouverte" jusqu'à ce que tu n'aies plus besoin de ton site. Lorsque tu fais des modifications
 dans ton éditeur de texte elles s'effectuent directement sur le site que t'as ouvert grace à **mkdocs serve**.  
 Faut quand même faire des `git push` si tu veux que ça s'affiche sur le vrai site. 2 fois dans la semaine tes 
modifications seront uploadées sur le grand server  et c'est là que tu les verras sur le vrai site.

### 3.3.3 Modules
Dans fabzero-modules tu vois les modules...et tu peux les modifier.   

## 3.4 Réduire la taille des images
Tu veux utiliser des images mais tu dois avoir des images pas trop lourdes. Un bon outil pour les 
éditer est [imagemagick](https://legacy.imagemagick.org/script/download.php).  
Les commandes de base: ![](images/mod1/img.jpg)  
tu peux trouver encore plus de commandes [here](https://imagemagick.org/script/command-line-tools.php).  

# 4. Checklist 
Juste pour vérifier que t'as fait ce qu'on t'a demandé:  
Faut cliquer sur **template** dans "evaluate your work" au niveau du module 1.  
Ou cliquer sur chaque module dans la liste des issues ( vous verrez ça surement dans un module ultérieur...   
![](images/mod1/cl.jpg)
- ☑ made a website  and deescribe how you did it
- ☑ introduced yourself
- ☑ documented steps for uploading files to the archive
- ☑ documented steps for compressing images and keeping storage space low
- ☑ documented how you are going to use project management principles
- ☑ pushed to the class archive