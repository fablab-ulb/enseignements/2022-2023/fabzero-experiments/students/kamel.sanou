# 3. Impression 3D
 Cette semaine nous avons suivi une formation et appris à utiliser une imprimante 3d. 
 Pour utiliser les machines au fablab il faut forcément une formation.  
 
## 1. Logiciel
 D'abord, j'ai appris ce qu'est un [slicer](https://en.wikipedia.org/wiki/Slicer_(3D_printing). 
 C'est un outil qui recueille des infos des objets 3d et les transmets à l'imprimante.  
 Aux fablabs, le slicer utilisé est [Prusaslicer](https://help.prusa3d.com/article/install-prusaslicer_1903).   

Après l'avoir installé, faut le configuer en suivant ces 
[instructions](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md). 
et l'intrface ressemble à ça:  
![](images/mod3/prusa.jpg)

## 2. Notions qui peuvent être bonnes à savoir
D'abord voici à quoi ressemble une machine  
![machine](images/mod3/machine.jpg)  
Plein de notions et d'infos nous furent données durant la formation. Je peux en citer certaines:  
La machine est soumise à certaines limites (n'imprime pas dans le vide, a partir d'un certain angle 
c'est mort) et cela est montré par le [torture test](https://www.thingiverse.com/thing:2806295/comments)
 parmi tant d'autres tests.  

 
### Au niveau de Prusaslicer 
Ajouter bordurepour empecher que la pièce se décolle quand la surface de contac avec la plaque est trop petite.
 Ca va créer une sorte de base à la fin.  
 
 Au niveau de l'épaisseur de paroi, mettre périmètre à 3 au lieu de 2.  
 
 Cliquer sur l'icone en bas à gauche qui ressemble à des couches/calques de feuilles pour voir des paramètres
 comme le temps que prend ton objet pour être imprimé. Et modifier les dimensions diminue le temps ofc.

Le motif de remplissage peut jouer aussi. Il peut être modifié dans **Print settings->Infill->fill pattern**
là tu peux voir des exemples de moif de remplissage.  
![motif](images/mod3/fillin.jpg)  

##### Petite parenthèse
hehe là tu peux voir les symboles des mesures de sécurité... dont l'un d'entre eux me fait penser 
à [Richrad Osman](https://www.imdb.com/name/nm0652066/)
 de [cette scène](https://www.youtube.com/watch?v=B6mCL6fuvVY)
 de [taskmaster](https://www.channel4.com/programmes/taskmaster)
![securitéffab](images/mod3/ard.jpg) ![richardtask](images/mod3/rich.jpg)

Des parties mobiles du mécanisme peuvent etre print en meme temps si leur montage est dur par exemple ou
bien on peut imprimer plusieurs parties en détail et les assembler pour avoir un truc full.  
Comme notre ....

## 3. Flexlink
Je l'ai imprimé et en vrai il est stiff mais toujours flexible.  
![flex](images/mod3/flexlink.jpg)  
![](images/mod3/flexion.jpg) 
Tu peux le trouver [là](files/flex.FCStd)
et j'ai fait une bete manivelle avec 2 vis.
![manive](images/mod3/maniv.jpg)...un peu flou

## 4. Catapulte
J'ai imprimé ma cuillère (voire [module 2]()) et ça ressemble à ça:
![](images/mod2/poeeeele.jpg)  

### Problème
Ma poele/cuillère n'est pas assez flexible pour revenir en position de départ quand on la pousse.
Aussi mes camarades et moi on n'a pas du tout les meme proportions donc aucune partie ne se met bien ensemble.

### Toutefois
Comme expliqué dans le module précédent, j'ai repris les paramètres du flexlink et j'ai refait
 une cuillère(plutôt pêle) pour la rendre plus fexible et j'ai eu ça:  
 ![pêle](images/mod3/pele.jpg)  
 ![peleflex](images/mod3/flex.jpg)
 
### Autre problème
La base de la cuillère/poêle s'est cassée or c'est important pour la fixer à la base de la catapulte
du coup faudrait la réimprimer.  
![cassé1](images/mod3/cass.jpg)  
![cassé2](images/mod3/cass2.jpg)

 
### Plan de la catapulte 
Il a un peu changé. Pour gagner en temps, Sami a réduit la surfacr de sa base et l'a imprimée avec 
les deux...poutres.  
Serge a imprimé une pièce en U qui recevrait ma pêle avec des trous ofc et des cylindres qui passeraient 
par ces trous ofc.  
Mariam a fait des sortes de vis qui permettent de maintenir les cylindres en place.  
On a eu ça comme résultat final  
 ![catapulte finale](images/mod3/cata1.jpg)
Il faut insérer un projectile dans le creux de la cuillère/pêle. Ensuite
appuyer sur la cuillère pour l'abaisser et la relacher pour lancer le truc. 
la barre au milieu pourra empecher la pêle d'aller plus loin que prévu et le projectile 
sera éjecté.  
Là vous avez un test peut concluant:  

<div style="padding:75% 0 0 0;
position:relative;"><iframe src="https://player.vimeo.com/video/817765348?h=ace5e8ea0f&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" 
frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;
" title="testcrash.mp4"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

Et un qui fonctionne mieux:  

<div style="padding:75% 0 0 0;position:relative;"><iframe 
src="https://player.vimeo.com/video/817759395?h=d94696e0ed&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" 
frameborder="0" allow="autoplay; fullscreen; 
picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;
" title="yeees.mp4"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>


##5 Sauf que ce n'est pas tout
Bien sûr ma cuillère était bancale, donc j'en ai modelisé et imprimé une [autre](files/autrepoele.FCStd) qui l'est encore plus.
La catapulte a fini par ressembler à ça:
![](images/mod3/pulteeuh.jpg)
![](images/mod3/pulteeuh2.jpg)

## 6.Checklist
- ☑ Explained what you learned from testing the 3D printers  
- ☑ Explained how you have identified your design parts parameters  
- ☑ Shown how you made your kit with words/images/screenshots  
- ☑ Included your original design files for 3D printing (both CAD and generic (STL, OBJ or 3MF) files)  
- ☑ Included images and description of your working mechanism.  


 
 
  
 
 