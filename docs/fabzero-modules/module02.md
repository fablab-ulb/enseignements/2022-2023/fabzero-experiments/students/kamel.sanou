# 2.Computer Aided Design CAD
Cette semaine j'ai appris à modéliser en 3D avec des outils CAD.

## 1. Logiciels

D'abord installer 

### 1.1 [Inkscape](https://inkscape.org/fr/)  
Un logiciel de dessins vectoriels. Un avantage du dessin vectoriel est que contrairement aux dessins "normaux" 
(raster), ils sont pas faits de pixels. Tu peux agrandir l'image vectorielle comme tu veux la qualité reste
pareille

### 1.2 [FreeCAD](https://www.freecad.org/) et [OpenSCAD](https://openscad.org/)
Fait de la modélisation en 3D...faudrait tester les 2 pour savoir lequel te plaît.
Il y a d'autres logiciels mais ces 2 là sont sans licence, du coup tu n'auras pas à te soucier de prix
qui augmentent.  
Pour ces logiciels il faut travailler en écriture paramètrique(consiste à mettre des paramètres genre surf=x*y avec 
x=3,y=2... ). Là quand tu veux modifier la surface tu modifies juste les paramètres et c'est plus facile.  


## 2. Tutos
 Faudrait apprendre à maîtriser les deux logiciels de modélisation... Ca tue pas lol
 
### 2.1 OpenSCAD
J'ai voulu créer un cube....Ca n'a **pas marché** car j'avais **oublié** un **point virgule ``;``**....
Je suis donc passé à:

### 2.2 FreeCAD
Je vais commencer par ça car c'est plus visuel et je suis plus à l'aise avec.
Suivons ce [tutoriel](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad/-/blob/main/FreeCAD.md)  

 **Je sais qu'il faut commencer par faire un body**...je sais tjrs pas comment le faire.   
 Ah en fait il faut se rendre dans la partie  **part design**
 ![](images/mod2/body1.jpg)  
 
 Après avoir créé des formes n'importe comment j'essaie de les supprimer et de vraiment suivre le tutoriel
. Pour ça, faut sélectionner la forme à supprimer et aller dans Edit->delete
![](images/mod2/del1.jpg)  

Je vais essayer de faire un pique aulieu d'un coeur.

J'ai réussi à former mon pique... je dois encore apprendre à gérer les degrés de liberté...Les degrés de liberté qui restent c'est ceux des 
paramètres que tu veux modifier  

![](images/mod2/sketcher.jpg)  


  
Ensuite faut fixer les paramètres en fonction des degrés de liberté qui restent
 Spreadsheet-> close dans tasks-> première icone en haut à gauche.  
 
 Dans 1ère colonne= noms  
 Dans 2ème colonne= valeurs puis attribuer un alias à ces valeurs.  
Surtout appuyer sur **entrer** après avoir écrit chaque alias.  

Après avoir fini le **""sketch""**, dans **""task""** cliquer sur **""close""** puis aller dans **""Part design""**
et extruder avec le cube juste à droite du ......chien...mouton?  

![](images/mod2/extruuude.jpg)  
Une fois que ce processus est compris, c'est plus simple de travailler et de crér d'autres pièces.


### 2.3 Projet catapulte  
Vu qu'on est censé faire un truc ensemble, on s'est dit....pourquoi pas une catapulte?
![](images/mod2/cata.jpg)

Moi je ferai la petite cuilère verte là.  
![](images/mod2/po.jpg)  

J'ai obtenu un truc qui ressemble à ce que je veux mais le mécanisme ne marche pas car la cuillère/poele 
n'est pas assez flexible.  
Il me faut créer une sorte de peigne sur lequel les dents auront des dimensions différentes. Ca me 
permettra de savoir quel ratio épaisseur/largeur donnera une bonne flexibilité.

#### 2.3.1 Problèmes rencontrés
[Ce tuto](https://youtu.be/TFpHx2bDjtc) m'a aidé
* j'ai commencé à faire un rectangle sur lequel je vais couper des trous.  
Faut donc faire un 2e sketch sur le pad. **Problème**, le sketch se fait en dessous.  
Il faut en fait cocher la case **reversed** dans les propriétés du pad.
![](images/mod2/reversed.jpg)
* Après avoir fait un trou, je me suis dit, autant les repliquer plusieurs fois.
Fallait appuyer sur **LinearPattern** et regler les paramètres à gauche en bas.
![](images/mod2/peigne.jpg)

### 2.4 Un nouvel espoir pour ma cuillère/poele/pêle
Au lieu d'imprimer mon "peigne" finalement, j'ai décidé d'y aller au talent 
car je n'avais plus trop de temps. Mon flexlink (j'en perle plus tard...) après impression etait 
plutot flexible du coup 
j'ai utilisé son ratio épaisseur/largeur pour que ma cuillère/poele maintenant devenue pêle le soit aussi.  
![pêle](images/mod2/pele.jpg)  

### 2.4' Ce n'est pas tout
Dans un dernier attempt de faire ma cuillère/poele/pêle plus à l'échelle j'ait fait ça 

### 2.5 Projet Ecotrophelia
Pour un projet sur lequel je travaille, on a besoin d'une étiquette à coller sur notre packaging, Jen ai fait une
mais de forme rectangulaire...  
Problème, le packaging n'est pas un cylindre
mais un cône tronqué => étiquette doit pas être un simple rectangle mais doit suivre une courbe.  
Alors je me suis dit....pq pas modéliser notre packaging et ... m'en servir pour modeliser mon étiquette.  

J'ai compris en faisant ça que si je veux créer des simples formes (ex: cylindre, cône ...) il faut 
**se rendre dans _Part_ (au lieu de _Part Design_)** et choisir la forme qui nous intéresse.  

Bref, j'ai juste créé la forme de mon paquet, mon étiquette ( en svg) était trop lourde/chargée, je 
n'arrivais pas à l'importer proprement; elle ne faisait que s'écraser et ne ressemblait en rien à
celle de départ. J'ai fini par utiliser photoshop.

## 3. Flexlinks
L'assignment consistait à créer un objet flexible puis utiliser celui d'un de mes camarades afin de créer
un compliant mechanism. 
![](images/mod2/flex.jpg)
Ca c'est [le mien](files/flex.FCStd)

## 4. Impression
Tout ce qui est de l'impression en 3D je vous le montre dans 
[ma documentation du module 3](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kamel.sanou/#fabzero-modules/module03/)

## 5. Licenses

Mettre des [licenses](https://creativecommons.org/about/cclicenses/) permet d'attribuer des droits à tes productions et à "définir" des règles  d'utilisation
vis à vis de leur utilisation.  
Il existe plusieurs types/versions de license. Celles utilisées dans ce module (les plus courantes j'imagine) sont les
**Creative commons**. On distingue:   

 |License|Description|
 |-------|-----------|
 |CC BY ![](images/mod2/c1.jpg) | Du crédit doit être accordé à l'auteur|
 |CC BY-SA![](images/mod2/c2.jpg)| Crédit à l'auteur et doit être mis sous license sous les même termes|
 |CC BY-NC ![](images/mod2/c3.jpg)| Crédit et la production peut être utilisée seulement à des fins non commerciales|
 |CC BY-NC-SA ![](images/mod2/c4.jpg)| Crédit; non commercial; mêmes termes|
 |CC BY-ND ![](images/mod2/c5.jpg)| Crédit et pas de dérivées ou d'adaptations du code sont autorisées|
 |CC BY-NC-ND ![](images/mod2/c6.jpg)| Crédit; non commercial; pas d'adaptations|
  
Moi je vais utiliser une [license CC BY](https://creativecommons.org/licenses/by/4.0/)
 où le crédit doit être attribué à l'auteur.  
Il faut juste écrire le fichier, l'auteur, la date et le type de license (mettre le lien aussi)

### Démarche
Ouvrir freeCAD et:  
* Dans **model** cliquer sur le nom du fichier
* Dans **property** en bas modifier les paramètres comme nom;type de licence;lien de la licence
![](images/mod2/cc.jpg)
![](images/mod2/cc2.jpg)

## 6. Checklist
- ☑ parametrically modelled some FlexLinks using 3D CAD software  
- ☑ shown how you did it with words/images screenshots  
- ☑included your original design files  
- ☑ included the CC license to your work  
- ☐ documented how you used other people's work and gave proper 
credit to complete your kit.