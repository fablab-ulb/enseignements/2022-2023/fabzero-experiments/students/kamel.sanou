# 5. Dynamique de groupe et projet final

Cette semaine j'ai travaillé en équipe et j'ai appris à....

## 1. Project analysis and design
Le concept de cette partie est l'étude d'une problématique.   
Il s'agit de représenter cette 
dernière sous forme d'un **arbre à problème** dont le tronc est le problème en question, les racines sont les causes 
et les branches en sont les conséquences.  
Ensuite faudrait transformer cet arbre à problème en **arbre à objectif** en:
* reprenant le prblème sous forme d'objectif
* remplaçant les racines par les actions à prendre pour résoudre ce problème
* remplaçantles conséquences par les débouchées de ces nouvelles racines  

Je l'ai fait avec 
[Sami](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/sami.el.hamdou/) et
[DAO Kodjo](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kodjo.dao/).  
Le template a été trouvé sur [Mural](https://www.mural.co/) et on s'est fixé comme problématique la surpopulation.  

| ![](images/mod5/problem2.jpg) |
|-------------------------------|
|Notre arbre à problèmes|  

 Nous y avons mis les causes et les conséquences qui nous semblaient les plus pertinentes.
 
 
|  ![](images/mod5/other2.jpg) |  
|------------------------------|  
|  Et là vous pouvez voir notre 2nd arbre |  

Là nous nous sommes inspiré des causes pour trouver des solutions. 
La plus efficace est d'office le **Thanos'snap** .

## 2. Group formation
Pour cette partie, chacun de nous devait se rendre en cours le mardi 
07 Mars avec un objet qui représenterait/aurait un lien
avec une problématique qui lui tient à coeur.

### 2.1 Mon objet
J'ai apporté une **loupe** . Ma thématique étant **l'exploitation des travailleurs/leurs mauvaises conditions/inégalité dans le monde du travail** .  
Le raisonnement derrière:   
Derrière les grandes entreprises se cachent de petits travailleurs qui sont surexploités et qui 
vient dans des conditions impossibles. Du coup la loupe représente un changement d'échelle.
Elle sert à regarder dans les grands instituts et à oberver/mettre en valeur les petits travailleurs.

### 2.2 Processus
Faut noter que tout ça se faisait dans une limite de temps imposée par Chloé qui avait une cloche.
Je vais brièvement expliquer ce qui s'est passé:  
#### 2.2.1. Never gonna
 On dépose nos objets un peu partout et chacun se promène et repère ceux qui pourraient être en rapport
avec sonn propre objet.

#### 2.2.2. Give
 On forme un cercle et chacun tient son objet en main et dit à haute voix de quel objet il 
s'agit (histoire que tout le monde soit au courant de à qui appartient quoi et que tout le monde
découvre ce qu'il a pas pu voir...).  

#### 2.2.3. You
 Chacun divague et va retrouver au moins 3 objets qui se rapprochent du sien...   
Moi je me suis rapproché de 
[Thibault](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/leonard.thibault/)
 qui avait un **microscope**. Sa thématique était plutot liée
à l'éducation des enfants mais après
 [Cédric](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/cedric.luppens/) 
nous a rejoint avec sa **craie** (representant les inégalités dans le
domaine de l'éducation=> le parfait pont entre mon thème et celui de Thibault.) Puis on a été rejoint
 par [Stanislas](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/stanislas.mondesir/)
qui avait un **cable éthernet** (lié aussi au manque d'éducation dans le domaine de l'informatique)  
![](images/mod5/oim2.jpg) 

#### 2.2.4. Up
 Par groupes de 4, on met ensemble les thèmes communs abordés par nos objets et on trouve une thématique 
sur laquelle on aimerait travailler.  
On s'est rendu compte que nos sujets abordaient plus des problèmes sociaux comparés aux autres qui étaient
plus d'ordre écologique. On a donc obtenu les thématiques suivantes:   
![](images/mod5/gf.jpg)
#### 2.2.5. Never gonna
 De là, établir sur une échelle de positionnement décisionnel à quel point on est chaud pour cette thématique.  
 Voici la notre:  
 ![](images/mod5/epp.jpg)

#### 2.2.6. Let you
 Après le choix du thème, chercher des problématiques et les énumérer à tour de rôle en commençant
chaque phrase par "**moi à ta place, je...**"  
On a eu ça:  
![](images/mod5/fe.jpg)

#### 2.2.7. Down
 Après avoir eu une liste de problématiques, chaque groupe envoie un membre dans d'autres groupes pour
avoir une autre perspective et avoir d'autres idées de problématique. Ca a donné:
![](images/mod5/pf.jpg)

## 3. Group dynamic
![](images/mod5/chloe.jpg)  
Là on était avec Chloé. On a appris plus sur la dynamique de groupe, l'importance de définir des rôles au sein du groupe
comme celui d'animateur durant les réunions ( s'assure que la réunion est 
fluide et tout le monde a la parole), de secrétaire, le type de gestion de temps....  
Elle nous a montré plain d'outils parmi lesquels 
* **La technique des 5 doigts** pour décider de qui assure un rôle par exemple: 2 personnes veulent être 
secrétaires? qu'elles lèvent la main et présentent un nombre de doigts qui représente à quel point elles 
sont motivées pour ce rôle.
* **Temperature check** : au début de chaque réu, chacun présente son bras soit en position élevée, mi-élevée 
soit carrément en bas => montre dans quel mood on se trouve. Ca permet aux membres du groupe de pas assumer 
qu'une personne est agacée par eux alors qu'elle s'est peut-être levée du mauvais pied.
* **Turage au sort**  où lorsqu'on a tous des idées de projet et qu'on sait pas laquelle
choisir on tire juste au sort l'une d'entre elles. C'est plutôt efficace lorsque toutes les idées 
sont bonnes et que peu importe l'outcome, ça ne dérangera personne.

## 4. Problem and objective of our group project
Du côté de l'organisation, on essaira de suivre les étapes pour avoir une réunion fluide à savoir:  
* météo d'entrée avec un T° check => savoir comment on se sent
* clarifier rôles de la réunion
* ordre du jour si ce n'est pas fait au préalable
* tâches; agenda-rôle
* météo de sortie  

Niveau communication, on a créé un serveur discord pour pouvoir discuter plus aisément.  
 
Quant aux prises de décisions, les moins cruciales se feront par température check;   

### Problematique et solutions
Ensuite on s'est retrouvé pour choisir une problématique et trouver une solution fixe qui 
pourrait marcher. On savait qu'on voulait trouver un moyen de porter plus d'intérêt 
et d'improve le secteur de l'éducation. Toutefois ce n'était pas assez précis. Denis nous a conseillé 
de se fixer une **forte** contrainte comme une localité par exemple.  
On a parlé de nos expériences à l'école secondaire et moi j'ai parlé du coup de l'éducation dans mon pays
le **Burkina Faso** . On s'est dit que ce serait intéressant de prendre ce pays, en plus j'ai des 
connaissances dessus.On en a fait un arbre à problèmes et un à solutions
 Et comme solution du coup on a pensé à un appareil qui pourrait être une 
bonne application au secteur de l'agriculture par exemple (un secteur beaucoup exploité dans mon pays).  
C'etait une bonne expérience dans l'ensemble où chacun a eu à participer et à se faire entendre

 
## Checklist
- ☑ presented the object you chose and the reason why you chose it   
- ☐ documented the list of potential problems identified 
 by your team at the end of the process  
- ☐ shared your experience of the brainstorming process 