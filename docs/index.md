## Foreword

Hello! my name is Kamel ! Let me welcome you to my blog!  
mdrrr je vais pas tout écrire en anglais


## About me

![](images/fleurs.jpg)
![](images/kamel_bg1)  
Salut, je m'appelle Kamel et j'ai 20 ans.  
Au momemnt où j'écris ça, je suis en BA3 en bioingénieur à l'ULB. Je fais ce blog dans le cadre du cours de PHYS-F517 intitulé "How To Make (Almost)
Any Experiment Using Digital Fabrication".  
 Durant mes temps libres, j'aime aller marcher, aller à la salle, dessiner.... J'aime beaucoup [DESSINER](https://www.artstation.com/harris58)
 


## My background

Je suis né à Ouagadougou au Burkina Faso. J'y ai grandi et étudié jusqu'à l'obtention de mon baccalauréat série D (sciences et littérature). 
J'étais toujours indécis à propos du choix de mes études, toutefois j'ai été [convaincu](https://www.larousse.fr/dictionnaires/francais/contraindre/18667)
de faire des études en bioingé. Ca reste une bonne expérience et je ne regrette pas avoir choisi bioingé même si un 
je pourrais m'épanouir dans un autre domaine 

## Previous work
Je n'ai pas beaucoup d'expérience dans le domaine de bioingés, du coup je vais plutôt vous faire part de mon expertise dans le graphisme


### Prisme

Je suis illustrateur pour le [webzine prisme ULB](https://prisme.ulb.be) entièrement écrit par les étudiants
 ,pour les étudiants (et tout le monde bien sûr).
 
 Ici quelques une de mes illustrations:
 

| ![](images/arme.jpg) |
| :--: |
|*numéro le plus récent sur [la paix et la guerre](https://prisme.ulb.be/tous-les-numeros/hiver-2023)|



| ![](images/trsprt2.jpg) |  
| :--: |  
| *numéro sur [le tourisme](https://prisme.ulb.be/tous-les-numeros/printemps-2022)* |


### Drawing for fun

Je dessine aussi en dehors du travail:  
**FEAST YOUR EYES!!!**  
![](images/jojo.jpg)  
![](images/gre.jpg) 
![](images/poke.jpg)  
![](images/sag.jpg)
![](images/fe.jpg)
![](images/cat.jpg)  
![](images/portrait.jpg)

La qualité des images est un peu pourrie du coup mdr.

### Ecotrophelia
![](images/food.jpg) ![](images/atwork.jpg)

Dans le cadre d'un cours de BA3 portant sur la **gestion de projet et projet de recherche**,
mes camarades et moi avons participé au concours **ECOTROPHELIA**. Il consiste à
confectionner un produit alimentaire écologique et innovant.  
Nous avons donc créé une crème glacée salée, végane à base de champignons (venant de la société 
[Eclo](https://eclo.farm/fr/)) et à consommer en tant qu'apéritif avec des crackers. Nous avons baptisé ce merveilleux produit 
[**Ice Cracker**](https://foodatwork.be/fr/contest/21)  
[Une video ici](https://youtu.be/dpLTZXCstLk)
